/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bclerc <bclerc@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/24 14:54:06 by bclerc            #+#    #+#             */
/*   Updated: 2021/08/31 23:00:02 by bclerc           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/pushswap.h"

int	isnumber(char *number)
{
	int	i;

	i = 0;
	while (number[i])
	{
		if (!ft_isdigit(number[i]) && (number[i] != '-'))
			return (0);
		i++;
	}
	return (1);
}

void	readlist(t_stack *stack)
{
	t_stack	*tmp;

	if (stack == NULL)
	{
		printf("Empty stack\n");
		return ;
	}
	tmp = stack;
	while (tmp->next)
	{
		ft_putnbr(tmp->value);
		ft_putchar(' ');
		tmp = tmp->next;
	}
	ft_putnbr(tmp->value);
	ft_putchar(' ');
	printf("\n");
}

void	free_stack(t_stack *stack)
{
	if (stack)
	{
		if (stack->next)
			free_stack(stack->next);
		free(stack);
		stack = NULL;
	}
}

void	exit_error(void)
{
	printf("Error\n");
	exit(1);
}

int	is_in_stack(int value, t_stack **stack)
{
	t_stack	*tmp;

	tmp = *stack;
	while (tmp)
	{
		if (tmp->value == value)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}
